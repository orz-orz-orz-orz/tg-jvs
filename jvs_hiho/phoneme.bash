#!/usr/bin/env bash

JVSDIR=./jvs_hiho_ver1
OUTDIR_JULIUS=./aligned_labels_julius/
OUTDIR_OPENJTALK=./aligned_labels_openjtalk/
JULIUS_HMM_PATH=./model/phone_m/jnas-mono-16mix-gid.binhmm

TMPDIR=./tmp

mkdir -p $OUTDIR_JULIUS
mkdir -p $OUTDIR_OPENJTALK
mkdir -p $TMPDIR

# install libraries
# pip install git+https://github.com/Hiroshiba/julius4seg

# get file names
names=$(cat ./filenames.txt)

mkdir $TMPDIR/text
paste <(echo "$names") <(cat ./voiceactoress100_spaced.txt) |\
    while read name text; do
        echo $text > $TMPDIR/text/$name.txt
    done

# for julius label
for person in $(ls -d $JVSDIR/jvs*/ | grep -o 'jvs[0-9][0-9][0-9]'); do
    echo $person

    mkdir $TMPDIR/audio
    find $JVSDIR/$person/parallel100/wav24kHz16bit -name '*.wav' |\
        parallel sox {} $TMPDIR/audio/{/} channels 1 rate 16k

    mkdir $OUTDIR_JULIUS/$person
    echo "$names" |\
    parallel julius4seg_segment \
        $TMPDIR/audio/{}.wav \
        $TMPDIR/text/{}.txt \
        $OUTDIR_JULIUS/$person/{}.lab \
        --hmm_model $JULIUS_HMM_PATH \

    # for failed files
    join -v 1 <(echo "$names") <(ls $OUTDIR_JULIUS/$person | xargs basename -s .lab) |\
    parallel julius4seg_segment \
        $TMPDIR/audio/{}.wav \
        $TMPDIR/text/{}.txt \
        $OUTDIR_JULIUS/$person/{}.lab \
        --hmm_model $JULIUS_HMM_PATH \
        --only_2nd_path \

    rm -r $TMPDIR/audio
done


# for OpenJTalk label
for person in $(ls -d $JVSDIR/jvs*/ | grep -o 'jvs[0-9][0-9][0-9]'); do
    echo $person
    mkdir $OUTDIR_OPENJTALK/$person

    mkdir $TMPDIR/audio
    find $JVSDIR/$person/parallel100/wav24kHz16bit -name '*.wav' |\
        parallel sox {} $TMPDIR/audio/{/} channels 1 rate 16k

    echo "$names" |\
    parallel julius4seg_segment \
        $TMPDIR/audio/{}.wav \
        $TMPDIR/text/{}.txt \
        $OUTDIR_OPENJTALK/$person/{}.lab \
        --hmm_model $JULIUS_HMM_PATH \
        --like_openjtalk \

    # for failed files
    join -v 1 <(echo "$names") <(ls $OUTDIC_OPENJTALK/$person | xargs basename -s .lab) |\
    parallel julius4seg_segment \
        $TMPDIR/audio/{}.wav \
        $TMPDIR/text/{}.txt \
        $OUTDIR_OPENJTALK/$person/{}.lab \
        --hmm_model $JULIUS_HMM_PATH \
        --like_openjtalk \
        --only_2nd_path \
        
    rm -r $TMPDIR/audio
done
