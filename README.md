# TG-JVS


## 切割方式
- 目前暫時使用 jvs-hiho 的方式進行切割以後可能會換掉

## 參考文獻
POS table 的轉換
- http://kanji.zinbun.kyoto-u.ac.jp/~yasuoka/publications/2020-09-05.pdf
UNIDIC MANUAL
- https://clrd.ninjal.ac.jp/unidic/UNIDIC_manual.pdf
読み仮名（ルビ）つき声優統計コーパス音素バランス文
-https://github.com/Hiroshiba/voiceactress100_ruby

## 一些遇到的問題
1. 在 POS tagging 使用的是 Unidic, 格式和其他的字典比較不同。
2. Unidic 的模型要另外下載 => 裡面的 left-id.def 和 right-id.def 好像放反了，要手動交換檔名
3. 要注意 Unidic 目前似乎不能使用 名詞,固有名詞,組織名， 目前都標成 名詞,固有名詞,一般
4. ubuntu 上的 open_jtalk 的輸出好像有問題，和預期的不一樣，輸出的 openjtalk alignment 會有一些不同，目前還沒有做轉換， silB, silE, sp, q 分別 對應到open_jtalk的 sil, sil, pau, cl。
5. 如果不自己做 alignment，使用原本附 的 label 有可能會比較好，包含有另外的無聲子音標記的樣子。
6. 可以自己抽 pitch 判斷是 voice or unvoiced.
7. 加詞典的指令是： /usr/local/libexec/mecab/mecab-dict-index -m ~/Downloads/unidic-cwj-202302_full/model.bin -d ~/Downloads/unidic-cwj-202302_full -u oov_tables.dic -f utf8 -t utf8 oov_tables.csv
